<h1>Libraries in profiles</h1>

This project pre-emptively checks for missing JavaScript/CSS Assets and looks in other locations - the active profile folder and a user configurable extra location.

<h2>REQUIREMENTS</h2>

Drupal 8 is required, Drupal 8.2.x or higher is suggested.

<h2>INSTALLATION</h2>
Install as you would normally install a contributed Drupal module. See the <a href='https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8'>Drupal 8 instructions </a> if required in the Drupal documentation for further information.

<h2>CONFIGURATION</h2>
Configuration can be accessed at `/admin/config/development/library_locations`.

<h2>FAQ</h2>
Any questions? Ask away on the issue queue or email: design@briarmoon.ca.

This project has been sponsored by:
<h3><a href="http://design.briarmoon.ca">BriarMoon Design</a></h3>
   Full service web development and design studio. Specializing in responsive, secure, optimized Drupal sites. BriarMoon Design can help you with all your Drupal needs including installation, module creation or debugging, themeing, customization, and hosting.
